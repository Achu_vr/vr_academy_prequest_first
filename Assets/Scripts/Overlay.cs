﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overlay : MonoBehaviour {

	WebCamDevice[] devices;
	WebCamDevice device;
	WebCamTexture m_WebcamTexture;
	public Material webcamMaterial;

	void Start () {
		devices = WebCamTexture.devices;
		device = devices [0];
		m_WebcamTexture = new WebCamTexture (device.name, 200, 200, 64);
		m_WebcamTexture.Play ();
	}

	void Update () {

		webcamMaterial.SetTexture ("_MainTex", m_WebcamTexture);

	}
}
