﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookRotationScript : MonoBehaviour {

	public GameObject mainCamera;
	public GameObject gettingXZPos;
	Vector3 posThisObject;
	float _y;

	void Start () {
		_y = this.transform.position.y;
	}

	void Update () {

		transform.rotation = Quaternion.LookRotation (mainCamera.transform.position - this.transform.position);
		posThisObject = new Vector3 (gettingXZPos.transform.position.x, _y, gettingXZPos.transform.position.z);
		this.transform.position = posThisObject;

	}
}
